<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Order;
use DB;
use App\Models\Cart;
use Illuminate\Http\Request;
use App\Http\Requests\KeranjangRequest;

class APIController extends Controller
{
    public function index(){
        $product = Product::all();

        return response($product);
    }
    public function getProductMakanan(){
        $product = Product::where('kategori', 'makanan')->get();

        return response($product);
    }
    public function getProductKue(){
        $product = Product::where('kategori', 'kue')->get();

        return response($product);
    }
    public function getProductPeralatan(){
        $product = Product::where('kategori', 'peralatan')->get();

        return response($product);
    }

    public function getCart(Request $request){
        $user = Cart::where('users_id', $request->users_id)->get();
        
        $user->load(['product', 'user']);

        return response($user);
    }

    public function postToCart(KeranjangRequest $request){
        $cart = new Cart();
        $cart->fill($request->all());
        $cart->users_id = $request->users_id;

        $cart->save();
        $cart->load(['product', 'user']);
        
        return response($cart);
    }

    public function order(Request $request){

        try{
            if ($request->file('image')) {
                $imageTempName = $request->file('image')->getPathname();
                $imageName = $request->file('image')->getClientOriginalName();
        
                $path =$request->file('image')->store(null, 'uploads');
                $data = Order::create($request->toArray());
                

                DB::table('orders')
                ->where('image', $imageTempName)
                ->update(['image' => $path]);



                return response()->json([
                    "success" => true,
                    "message" => "successfull",
                    "file" => $path
                ]);

            }else{
               $data = Order::create($request->toArray());
               return response()->json([
                "success" => true,
                "message" => "successfull",
            ]);

           }


       }catch(Exception $error){
        return response()->json([
            'status_code' => 500,
            'message' => 'Failed',
            'error' => $error,
        ]);
    }


}

}
